=============
Dokumentation
=============

Einstieg
========

Die Dokumentation ist ein wichtiger, wenn nicht sogar der wichtigste
Teil eines Projekts.

Hier möchte ich dem Leser wichtige Konzepte rund um die Dokumentation
näherbringen.

Dokumentationstaktiken
======================

Die nachfolgenden Dokumentationstaktiken finden im Zusammenhang mit
der Niederschrift von Anforderungen seitens der Auftragsgeber
Anwendung.

.. glossary:: 

   Lastenhefte
      Bei einem Lastenheft handelt es sich um ein Dokument, welches
      detaillierte Spezifikationen beschreibt, sowohl sachlich, wie auch
      technisch, und lässt auch kategorisierte Anforderungen zu.

   User Stories
      User Stories behandeln explizit die Perspektive des Endnutzers auf
      die Applikation. Diese sind in kurzer Form gegeben und behandeln
      möglichst grundlegende Funktionen. Sie werden oft in der "Als
      [Benutzerrolle] möchte ich [eine Aktion], um
      [einen Nutzen zu erhalten]" Form beschrieben.

   Use Cases
      Use Cases sind spezifische Szenarien, in welchen welchen das Produkt
      sich auf eine bestimmte Art und Weise Verhalten soll. Sie legt Fokus
      auf die Interaktion zwischen dem Produkt und dem Nutzer und liefert
      leicht verständliche Beispiele zur Funktionsweise des Produktes.

   Prototypen
      Kleine Prototypen ermöglichen die frühe Ausmärzung möglicher
      Missverständnisse und sind ebenfalls ein erster Schritt in der
      Umsetzung, was einen Fortschritt in der Umsetz selbst bedeutet.

   Datenmodelle
      Datenmodelle (zum Beispiel: Entity-Relationship-Diagramm)
      ermöglichen eine visuelle Darstellung der Struktur einer Datenbank
      oder geben auch Auskunft über den Aufbau eines späteren Produktes.

Wichtige Konzepte
=================

Personas
--------

Personas sind Spezifikationen, welche die Zielgruppen des Produktes
repräsentieren. Sie ermöglichen das Hineinversetzen in einen Kunden und
das Nachvollziehen dessen Bedürfdnisse und so kann das Produkt besser
an die Anforderungen des Kunden angepasst werden.

Personas können bereits in der Informationsphase ausgemacht werden und
mit dem Kunden ausgearbeitet werden, was so die Personas auch für die
Planung und das Fällen von funktionsspezifischen Entscheidungen
vereinfacht, da weniger Mutmassungen bezüglich der Ziele des Produktes
notwendig sind.

Oftmals werden Nutzermodelle mittels folgender Vorlage formuliert:
"Persona A, ein erfahrener Benutzer, benötigt Funktion X, um seine
Arbeitsabläufe effizienter zu gestalten".

Gut formulierte Anforderungen
-----------------------------

Details in Anforderungen
^^^^^^^^^^^^^^^^^^^^^^^^

Es ist überaus wichtig, dass die Anforderungen möglichst tiefschürfend
festlegen, wie das Produkt strukturiert sein soll und wie es
funktionieren soll. Dies bedeutet, dass die Struktur des Produktes und
seiner Komponenten anhand der Anforderungen erahnbar oder gar eindeutig
sein sollte.

Eindeutigkeit der Anforderungen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Bei der Überprüfung einer Anforderung auf deren Eindeutigkeit ist diese
mit anderen Anforderungen zu überprüfen und sicherzustellen, dass diese
nicht im Widerspruch zu den übrigen Anforderungen steht, also
beispielsweise eine Anforderungen bestimmte Technologien voraussetzt,
während eine andere Anforderungen andere, konfliktbehaftete
Technologien voraussetzen.

Messbarkeit der Anforderungen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Es gilt sicherzustellen, dass alle Anforderungen eindeutig als
"erreicht" oder "nicht erreicht" eingestuft werden können. Dies ist
wichtig, da damit Meilensteine gesetzt werden können, was
die Durchführung von beispielsweise Scrum Stints vereinfacht, und so
die Auswertung eines Auftrages anhand einer Anforderung eindeutig und
klar ist.

Anforderungen katalogisieren
----------------------------

Katorisierung anhand Anforderungstypen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Anforderungen können mittels Anforderungstypen kategorisiert
werden. Gängige Typen sind zum Beispiel: Funktionale Anforderungen,
entwicklungsspezifische Anforderungen und repräsentative Anforderungen.

Kategorisierung anhand Relevanz
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Im Weiteren, ins besondere im Zusammenhang mit Meilensteinen, können
Anforderungen anhand deren Relevanz im Generellen oder in Relation zu
anderen Anforderungen.

Aufwandsschätzung Auftrag
-------------------------

Analyse der Anforderungen
^^^^^^^^^^^^^^^^^^^^^^^^^

Alle Anforderungen sollten unter die Lupe genommen werden und es soll
festgestellt werden, ob, und, gegebenenfalls, welche Anforderungen
Komplikationsrisiken mit sich bringen, also den Bedarf an einem
Zeitpuffer nahelegen. Ausserdem gilt es, jene Aufträge mit hoher
Relevanz zu priorisieren.

Zeitrahmen für Umsetzung
^^^^^^^^^^^^^^^^^^^^^^^^

Die Implementation einer Anforderung bring manche zeitlichen Faktoren
mit sich. Diese währen zum Beispiel die Integration in eine bestehende
Infrastruktur oder die Einarbeit in eine neue Technologie im Rahmen des
Auftrages. Es gilt, diese Faktoren in die Zeitschätzung einzubeziehen.

Zeitrahmen für die Dokumentation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nach Umsetzung oder währendessen sollte dokumentiert werden, was wann
geändert wurde und weshalb es geändert wurde. Es sollte besser Zeit im
Überfluss, als spärlich, für die Dokumentation beansprucht werden, da
die Dokumentation für andere Entwickler am Projekt enorm relevant sein
kann, wenn es darum geht, das Projekt kennenzulernen oder Änderungen
und deren Hintergrund nachzuvollziehen.

Einbezug eines Puffers
^^^^^^^^^^^^^^^^^^^^^^

Für den Fall von unerwarteten Komplikationen Puffer einzuplanen, damit
genug Zeit besteht, diese aufzulösen und die Deadline einzuhalten.

Qualität der Anforderungen
--------------------------

Regelmässigkeit der Absprachen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Ansprache im Bezug auf die Anforderungen und Plänen eines Projektes
sollte regelmässig gehalten werden und möglichst viele Illustrationen
und Demonstrationen beinhalten und sollte, falls notwendig,
überarbeitet werden.

Anpassungen nach Notwendigkeit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Anforderungen sollten im Falle von Überarbeitungen umgehend angepasst
werden und sollten, im Rahmen der Planung, auch an die gegebenen,
technischen Aspekte angepasst werden.