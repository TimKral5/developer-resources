==========================
Begriffe rund um Netzwerke
==========================

Adressierung
============

.. glossary::

   MAC Adresse
      MAC Adressen sind Teil der OSI Layer 2, sind 48 Bit gross und
      sind bei jedem Gerät fix eingerichtet.

   IP Adresse
      IP Adressen sind Identifikationsnummern, welche ab der OSI
      Layer 3 von den dazugehörigen Netzwerkkomponenten verwendet wird.

   IPv4 Adresse
      Der IPv4 Standart schreibt eine Grösse von 32 Bit vor.

   IPv6 Adresse
      Der IPv4 Standart schreibt eine Grösse von 128 Bit vor.

Netzwerkkomponenten
===================

.. _networking-terminology-switching_table:

.. glossary:: 
   Switching Tabelle
      Eine Tabelle, welche die Ports und alle direkt oder indirekt
      darüber erreichbaren Geräte anhand deren MAC-Adresse speichert.