===================
MariaDB Constraints
===================

.. glossary::

   Primary Key

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <pk-col> <type> <attr> PRIMARY KEY <attr>
         );

         CREATE TABLE sample (
            <pk-col> <type> <attr>,
            PRIMARY KEY (<pk-col>)
         );

         CREATE TABLE sample (
            <pk-col> <type> <attr>,
            CONSTRAINT <pk-name> PRIMARY KEY (<pk-col>)
         );
      
   Foreign Key

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <fk-col> <type> <attr>,
            FOREIGN KEY (<fk-col>) REFERENCES <ref-table> (<ref-pk-col>)
         );

         CREATE TABLE sample (
            <fk-col> <type> <attr>,
            CONSTRAINT <fk-name> FOREIGN KEY (<fk-col>) REFERENCES <ref-table> (<ref-pk-col>)
         );
   
   Not Null

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <col> <type> <attr> NOT NULL <attr>
         );

   Unique

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <col> <type> <attr> UNIQUE <attr>
         );

         CREATE TABLE sample (
            <col> <type> <attr>
            UNIQUE (col)
         );

         CREATE TABLE sample (
            <col> <type> <attr>
            CONSTRAINT <uq-name> UNIQUE (col)
         );
   
   Check

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <col> <type> <attr>
            CHECK (<condition>)
         );

         CREATE TABLE sample (
            <col> <type> <attr>
            CONSTRAINT <chk-name> CHECK (<condition>)
         );

   Default

      Implementation:

      .. code-block:: sql

         CREATE TABLE sample (
            <col> <type> <attr> DEFAULT <def-val>
         );

         CREATE TABLE sample (
            <col> <type> <attr> DEFAULT <func>
         );
      
   Indices

      *Platzhalter*