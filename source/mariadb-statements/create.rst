=========================
MariaDB CREATE Statements
=========================

CREATE DATABASE
===============

.. code-block:: sql

   CREATE [OR REPLACE] DATABASE [IF NOT EXISTS] db_name <create_definition>;
   CREATE [OR REPLACE] SCHEMA [IF NOT EXISTS] db_name <create_definition>;

   <create_definition>
      [DEFAULT] CHARACTER SET [=] <charset_name>
      [DEFAULT] COLLATE [=] <collation_name>
      COMMENT [=] 'comment';

CREATE TABLE
============

.. code-block:: sql

   CREATE [OR REPLACE] [TEMPORARY] TABLE [IF NOT EXISTS] <table_name>
      (<column_definition>, ...) [<table_options>] [<partition_options>];

   CREATE [OR REPLACE] [TEMPORARY] TABLE [IF NOT EXISTS] <table_name>
      [(<column_definition>, ...)] [<table_options>] [<partition_options>]
      <select_statement>;

   CREATE [OR REPLACE] [TEMPORARY] TABLE [IF NOT EXISTS] <table_name>
      [(] LIKE <old_table> [)];

   <column_definition>
      <column_name> <data_type> [<attribute> ...] [DEFAULT <default_value>]

   <table_options>
      // TODO

   <partition_options>
      // TODO


CREATE USER
===========

.. code-block:: sql

   CREATE [OR REPLACE] USER [IF NOT EXISTS] <user_definition>[, ...];

   <user_definition>
      <username> [<authentication_options>]

   <authentication_options>
      IDENTIFIED BY <password>
   or
      IDENTIFIED BY PASSWORD <password_hash>
   or
      IDENTIFIED WITH <authentication_rule> [OR <authentication_rule>]

CREATE PROCEDURE
================

*Platzhalter*