=======================
MariaDB SHOW Statements
=======================

SHOW
====

.. rubric:: SHOW TABLES

.. code:: sql

   SHOW [FULL] TABLES [FROM <db_name>] [LIKE <db_name_pattern>];


.. rubric:: SHOW DATABASES

.. code-block:: sql

   SHOW DATABASES [LIKE <db_name_pattern>];
   SHOW SCHEMAS [LIKE <db_name_pattern>];


.. rubric:: SHOW CREATE DATABASE

.. code-block:: sql
   
   SHOW CREATE DATABASE <db_name>;
   SHOW CREATE SCHEMA <db_name>;


.. rubric:: SHOW CREATE TABLE

.. code-block:: sql

   SHOW CREATE TABLE <table_name>;

DESCRIBE
========

.. rubric:: DESCRIBE TABLE

.. code-block:: sql

   DESCRIBE <table_name>;
   DESC <table_name>;

.. rubric:: DESCRIBE SQL STATEMENT

.. code-block:: sql

   DESCRIBE <sql_statement>;
   DESC <sql_statement>;