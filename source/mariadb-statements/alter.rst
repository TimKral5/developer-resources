========================
MariaDB ALTER Statements
========================

ALTER DATABASE
==============

.. code-block:: sql

   ALTER DATABASE [<db_name>] <alter_specification>;

   <alter_specification>
      [DEFAULT] CHARACTER SET [=] <charset_name>
      [DEFAULT] COLLATE [=] <collation_name>
      COMMENT [=] 'comment';

ALTER TABLE
===========

.. code-block:: sql

   ALTER TABLE ADD [COLUMN] [IF NOT EXISTS] <column_definition>;
   ALTER TABLE DROP [COLUMN] [IF EXISTS] <column_name>;

   ALTER TABLE ADD [CONSTRAINT [<constraint_symbol>]] [IF NOT EXISTS] <constraint_definition>;
   ALTER TABLE DROP CONSTRAINT [IF EXISTS] <constraint_symbol>;

ALTER USER
==========