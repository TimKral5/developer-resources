=========================
MariaDB SELECT Statements
=========================

SELECT FROM
===========

.. code-block:: sql

   SELECT [ALL | DISTINCT | DISTINCTROW] [HIGH_PRIORITY]
      [STRAIGHT_JOIN] [SQL_SMALL_RESULT] [SQL_BIG_RESULT]
      [SQL_BUFFER_RESULT] [SQL_CACHE | SQL_NOCACHE]
      [SQL_CALC_FOUND_ROWS] <select_expr>[, ...]
   [FROM <table>
      [WHERE <where_condition>]
      [GROUP BY <expr> [(ASC | DESC)][, ...] [WITH ROLLUP]]
      [HAVING <where_condition>]
      [ORDER BY <expr> [(ASC | DESC)][, ...]]
      [
         (
            LIMIT <[<offset>,] <row_count> |
            <row_count> OFFSET <offset>> |
               [OFFSET <offset> ROW | ROWS ]
               [FETCH (FIRST | NEXT) <count> (ROW | ROWS) ]
         )
      ]
      [
         (
            INTO OUTFILE <file> [CHARACTER SET <charset>] [<export_options>] |
            INTO DUMPFILE <file> |
            INTO <variable>[, ...]
         )
      ]
      [(
         FOR UPDATE <lock_option> |
         LOCK IN SHARE MODE <lock_option>
      )]
   ]

WHERE und HAVING
================

WHERE Blöcke beziehen sich auf individuelle Tabellenzeilen, während
HAVING sich auf Gruppen bezieht.