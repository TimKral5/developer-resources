=======================
MariaDB DROP Statements
=======================

DROP DATABASE
=============

.. code-block:: sql

   DROP DATABASE [IF EXISTS] <db_name>;
   DROP SCHEMA [IF EXISTS] <db_name>;

DROP TABLE
==========

.. code-block:: sql

   DROP [TEMPORARY] TABLE [IF EXISTS] <table_name> [, ...];

DROP USER
=========

.. code-block:: sql

   DROP USER [IF EXISTS] <username> [, ...];