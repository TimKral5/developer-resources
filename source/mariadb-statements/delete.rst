=========================
MariaDB DELETE Statements
=========================

Single-Table DELETE
===================

.. code-block:: sql

   DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM <table_references>
      [WHERE <where_condition>] [ORDER BY ...] [LIMIT <row_count>]
      [RETURNING <select_expr> [, ...]];

Multiple-Table DELETE
=====================

.. code-block:: sql

   DELETE [LOW_PRIORITY] [QUICK] [IGNORE] <table_name> [, ...]
   FROM <table_references>
      [WHERE <where_condition>] [ORDER BY ...] [LIMIT <row_count>]
      [RETURNING <select_expr> [, ...]];