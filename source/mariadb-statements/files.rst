===============
MariaDB Dateien
===============

Daten importieren
=================

.. code-block:: sql

   LOAD DATA INFILE <file>
      INTO TABLE <table>
   [CHARACTER SET <charset>]
   [
      FIELDS
         [TERMINATED BY <string>]
         [ENCLOSED BY <string>]
         [LINES TERMINATED BY <string>]
   ];

Daten exportieren
=================

.. code-block:: sql

   SELECT <col> INTO OUTFILE <file>
      [CHARACTER SET <charset>]
      [COLUMNS
         [
            [TERMINATED BY <string>]
            [[OPTIONALLY] ENCLOSED BY <char>]
            [ESCAPED BY <char>]
         ]
         [LINES
            [STARTING BY <string>]
            [TERMINATED BY <string>]
         ]
      ];

Queries importieren
===================

.. code-block:: sql

   SOURCE <file>;

Queries importieren
===================

.. code-block:: bash

   mysql -u <user> -p[<password>] [<database>] < <source>

Queries exportieren
===================

.. code-block:: bash
   
   mysqldump -u <user> -p[<password>] [<database>] > <destination>
