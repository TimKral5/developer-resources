==========================
MariaDB BuiltIn Funktionen
==========================

Allgemeine Funktionen
=====================

.. glossary::

   :code:`CAST`
      
      .. code-block:: sql

         CAST(<value> AS <type>)

String Funktionen
=================

.. glossary::

   :code:`CONCAT`

      .. code-block:: sql

         CONCAT(<str1>, <str2>[, ...])
      
      Fügt die angegebenen Strings aneinander an.

   :code:`LENGTH`

      .. code-block:: sql

         LENGTH(<str>)

      Gibt die Länge eines Strings in Bytes aus.

   :code:`SUBSTRING`

      .. code-block:: sql

         SUBSTRING(<str>, <from>, <len>)

      Gibt einen Ausschnitt eines Strings mit einer gewählten
      Startposition und Länge aus dem Urspünglichen zurück.

   :code:`UPPER`

      .. code-block:: sql

         UPPER(<str>)
         UCASE(<str>)
      
      Wechselt im String alle Kleinbuchstaben mit Grossbuchstaben.

   :code:`LOWER`

      .. code-block:: sql

         LOWER(<str>)
         LCASE(<str>)
      
      Wechselt im String alle Grossbuchstaben mit Kleinbuchstaben.
