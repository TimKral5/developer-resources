==================
MariaDB Datentypen
==================

Numeric Data Types
==================

.. glossary::

   :code:`TINYINT`
      .. code-block:: sql
         
         TINYINT[(n)] [SIGNED | UNSIGNED | ZEROFILL]

      Eine Ein-Byte-Ganzzahl (1 Byte) (Wertebereich: -128 bis 127, bzw.
      unsigned 0 bis 255).

   :code:`BOOLEAN`
      .. code-block:: sql

         BOOLEAN, BOOL

      Eine Variabel mit zwei Zuständen: False (0) und True (!0).
   
   :code:`SMALLINT`
      .. code-block:: sql
         
         SMALLINT[(n)] [SIGNED | UNSIGNED | ZEROFILL]
      
      Eine Zwei-Byte-Ganzzahl (2 Byte) (Wertebereich: -32768 bis 32767,
      bzw. unsigned 0 bis 65535).

   :code:`MEDIUMINT`
      .. code-block:: sql
         
         MEDIUMINT[(n)] [SIGNED | UNSIGNED | ZEROFILL]
      
      Eine Drei-Byte-Ganzzahl (3 Byte) (Wertebereich: -8388608 bis
      8388607, bzw. unsigned 0 bis 16777215).
   
   :code:`INT`
      .. code-block:: sql
         
         INT[(n)] [SIGNED | UNSIGNED | ZEROFILL]
      
      Eine Vier-Byte-Ganzzahl (4 Byte) (Wertebereich: -2147483648 bis
      2147483647, bzw. unsigned 0 bis 4294967295).
   
   :code:`BIGINT`
      .. code-block:: sql
         
         BIGINT[(n)] [SIGNED | UNSIGNED | ZEROFILL]
      
      Eine Acht-Byte-Ganzzahl (8 Byte) (Wertebereich: :math:`-2^{63}`
      bis :math:`2^{63} - 1`, bzw. unsigned 0 bis :math:`2^{64} - 1`).

String Data Types
=================

.. glossary::
   :code:`BINARY`

      .. code-block:: sql

         BINARY(n)
      
      Speichert Binary Strings in einem Buffer, welcher eine feste
      grösse an Bytes (n) aufweist.

   :code:`BLOB`

      .. code-block:: sql

         BLOB[(n)]
      
      Ein Binary Large Object Behälter mit einer Grösse bis zu
      65,535 Bytes.

   :code:`CHAR`

      .. code-block:: sql

         [NATIONAL] CHAR[(n)] [CHARACTER SET <charset>] [COLLATE <collation>]

      Speichert Text mit einer festen Länge.

   :code:`CHAR BYTE`

      Alternativer Name für :code:`BINARY`

   :code:`ENUM`

      .. code-block:: sql
      
         ENUM(<val>[, ...]) [CHARACTER SET <charset>] [COLLATE <collation>]
      
      Ein String, welcher einen Wert aus einer vorgegebenen Liste
      besitzen kann.

   :code:`INET4`

      .. code-block:: sql

         INET4
      
      Ein Datentyp, welcher IPv4 Adressen zu einem 4 Byte Binary String
      zusammenfasst, diese also so effizient wie möglich speichert.

   :code:`INET6`

      .. code-block:: sql

         INET6
      
      Ein Datentyp, welcher IPv6 Adressen zu einem 16 Byte Binary
      String zusammenfasst, diese also so effizient wie möglich
      speichert.

   :code:`JSON`

      .. code-block:: sql

         JSON
      
      JSON ist im Grundsatz ein Alias für :code:`LONGTEXT`, nimmt
      jedoch nur valides JSON an.

   :code:`MEDIUMBLOB`

      .. code-block:: sql

         MEDIUMBLOB
      
      Ein Binary Large Object (BLOB) Körper, welcher eine Länge von bis
      zu 16,777,215 Bytes haben kann.

   :code:`MEDIUMEXT`

      .. code-block:: sql

         MEDIUMTEXT [CHARACTER SET <charset>] [COLLATE <collation>]
      
      Ein Textkörper mit einer Länge von bis zu 16,777,215 Zeichen
      (ASCII).

   :code:`LONGBLOB`

      .. code-block:: sql

         LONGBLOB

      Ein Binary Large Object (BLOB) Körper, welcher eine Länge von bis
      zu 4,294,967,295 Bytes haben kann.

   :code:`LONGTEXT`

      .. code-block:: sql

         LONGTEXT [CHARACTER SET <charset>] [COLLATE <collation>]
      
      Ein Textkörper mit einer Länge von bis zu 4,294,967,295 Zeichen
      (ASCII).

   :code:`ROW`

      .. code-block:: sql

         ROW (<field> <type> [, ...])

      *Platzhalter*

   :code:`TEXT`

      .. code-block:: sql

         TEXT [CHARACTER SET <charset>] [COLLATE <collation>]
      
      Ein Textkörper mit einer Länge von bis zu 65,535 Zeichen (ASCII).

   :code:`TINYBLOB`

      .. code-block:: sql

         TINYBLOB

      Ein Binary Large Object (BLOB) Körper, welcher eine Länge von bis
      zu 225 Bytes haben kann.

   :code:`TINYTEXT`

      .. code-block:: sql

         TINYTEXT [CHARACTER SET <charset>] [COLLATE <collation>]

      Ein Textkörper mit einer Länge von bis zu 255 Zeichen (ASCII).

   :code:`VARBINARY`

      .. code-block:: sql

         VARBINARY(n)

      Ein Binärdaten-String mit variabler Länge, nimmt also (im Rahmen
      der maximalen Länge (n)) nur die notwendige Menge an Speicher in
      Anspruch.

   :code:`VARCHAR`

      .. code-block:: sql

         [NATIONAL] VARCHAR(n) [CHARACTER SET <charset>] [COLLATE <collation>]

      Ein String mit variabler Länge, nimmt also (im Rahmen der
      maximalen Länge (n)) nur die notwendige Menge an Speicher in
      Anspruch.

   :code:`UUID`

      .. code-block:: sql

         UUID

      Speichert UUIDs nach [ `RFC 4122 <https://www.rfc-editor.org/rfc/rfc4122>`_ ].