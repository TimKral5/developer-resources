=========================
MariaDB UPDATE Statements
=========================

Single-Table UPDATE
===================

.. code-block:: sql

   UPDATE [LOW_PRIORITY] [IGNORE] <table_name>
      SET <col> = <new_val> [, ...]
   [WHERE <where_condition>]
      [ORDER BY ...] [LIMIT <row_count>];

Multiple-Table UPDATE
=====================

.. code-block:: sql

   UPDATE [LOW_PRIORITY] [IGNORE] <table_name>
      SET <col> = <new_val> [, ...]
   [WHERE <where_condition>];