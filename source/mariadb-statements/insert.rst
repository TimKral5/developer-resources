=========================
MariaDB INSERT Statements
=========================

INSERT INTO
===========

.. code-block:: sql

   INSERT [LOW_PRIORITY | HIGH_PRIORITY] [IGNORE]
      [INTO] <table_name> [<col_name> [, ...]]
      VALUES (<col_value> [, ...]) [, ...];

INSERT INTO SELECT
==================

.. code-block:: sql

   INSERT [LOW_PRIORITY | HIGH_PRIORITY] [IGNORE]
      [INTO] <table_name> [<col_name> [, ...]]
      <select_statement>;