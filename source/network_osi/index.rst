==========
OSI Modell
==========

.. toctree:: 
   :caption: Hauptartikel
   :maxdepth: 1
   :glob:

   osi_model


.. toctree::
   :caption: Übrige Beiträge
   :maxdepth: 1
   :glob:

   ethernet_frame
   ip_header
   tcp_segment
   udp_segment