========================
Internet Protocol Header
========================

.. Die übrige Nutzlast beträgt bis zu 1480 Byte.

Aufbau
======

IPv4 (gemäss `RFC 791 <https://www.rfc-editor.org/rfc/rfc791>`_)
------------------------------------------------------------------------------------------

#. Version (4 bits)
#. Internet Header Length (IHL) (4 bits)
#. Type of Service (ToS) (8 bits)

   #. Precedence (3 bits)
   #. Delay (1 bit)
   #. Throughput (1 bit)
   #. Relibility (1 bit)
   #. Reserved for Future Use (2 bits)

#. Total Length (16 bits)
#. Identification (16 bits)
#. Flags (3 bits)

   #. Reserved (1 bit)
   #. Don't Fragment (DF) (1 bit)
   #. More Fragments (MF) (1 bit)

#. Fragment Offset (13 bits)
#. Time to Live (TTL) (8 bits)
#. Protocol (8 bits)
#. Header Checksum (16 bits)
#. Sender IP-Address (32 bits)
#. Destination IP-Address (32 bits)
#. Options (if :math:`IHL > 5` : :math:`(IHL - 5) * 4 \text{bytes}` )

Details
=======

.. glossary::

   Version
      Die Version gibt die verwendete Spezifikation im übrigen Header an,
      dies wäre bei IPv4 Binär 4 (0100) oder bei IPv6 Binär 6 (0110).

   Internet Header Length (IHL)
      Die IHL gibt Auskunft über die Grösse des Headers. Die Grösse wird
      in 4-Byte Schritten angegeben, die Grösse rechnet sich also so:

      :math:`s = IHL * 4 \text{bytes}`

   Type of Service (ToS)
      *Platzhalter*

   Total Length
      Gibt die Länge des Headers in Kombination mit der Nutzlast an.

   Identification
      Der Identifier ist bei fragmentierter Übertragung von Belang, da er
      angibt, um welches Fragment es sich handelt.

   Flags
      Die nachfolgenden Attribute gibt Auskunft darüber, ob die aktuelle
      Übertragung fragmentiert wurde, oder ob das aktuelle Paket das
      letzte fragment ist.

      Don't Fragment (DF)
         Diese Flag gibt an, ob die Daten fragmentiert wurden (0), oder
         nicht (1).

      More Fragments (MF)
         Diese Flag gibt an, ob dies das letzte Fragment ist (0), oder
         nicht (1).

   Time to Live (TTL)
      *Platzhalter*

   Protocol
      Das Protokoll beschreibt das Format der nächsten Layer (gemäss
      `RFC 790 <https://www.rfc-editor.org/rfc/rfc790>`_).

   Header Checksum
      *Platzhalter*

   Source IP-Address
      *Platzhalter*

   Destination IP-Address
      *Platzhalter*

   Options
      *Platzhalter*