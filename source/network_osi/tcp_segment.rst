============
TCP Segmente
============

Aufbau
======

#. Zielport (16 Bit / 2 Byte)
#. Senderport (16 Bit / 2 Byte)
#. Sequenznummer (32 Bit / 4 Byte)
#. Folgesegmentnummer / Acknowledgment Number (32 Bit / 4 Byte)
#. Datenoffset (4 Bit)
#. Reserviert (4 Bit)
#. Flags (8 Bit / 1 Byte)

   #. Congestion window reduced (CWR)
   #. ECE
   #. URG
   #. ACK
   #. PSH
   #. RST
   #. SYN
   #. FIN

#. Fenstergrösse (16 Bit / 2 Byte)
#. Prüfsummme (16 Bit / 2 Byte)
#. Offset in der Sequenznummer
#. Optionen (grösse nach Datenoffset: :math:`offs \cdot 32` )

Details
=======