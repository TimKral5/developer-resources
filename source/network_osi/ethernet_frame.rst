==============
Ethernet Frame
==============

.. Die übrige Nutzlast beträgt bis zu 1480 Byte.

Aufbau
======

IEEE 802.3 only
---------------

#. Preamble (7 octets)
#. Start Frame Delimiter (1 octets)
#. Destination Address (6 octets)
#. Source Address (6 octets)
#. Length (2 octets)
#. Data / Payload (max. 1500 bytes)
#. Frame Checksum (CRC) (4 bytes)

Details
=======
.. glossary::

   Preamble
      Die Präambel ist ein Überbleibsel aus Zeiten, in welchen die
      Geräte, die die Pakete empfangen, einen Moment brauchten, um den
      Lesevorgang zu starten, was zur Folge haben konnte, dass ein Paar
      Bits verloren gehen, weshalb die Präambel als Lösung einfach als
      Indikator eines eingehenden Paketes dient.

      Die Präambel enthält also einfach ein alternierendes Muster
      (0 und 1), beginnend mit Binär 1.

   Start Frame Delimiter (SFD)
      Der SFD ergänzt das alternierende Muster der Präambel, ersetzt
      aber die 0 am Schluss mit einer 1 (also 10101011).

   Destination Address
      Die Zieladresse ist die MAC-Adresse des nächsten Netzwerkknoten
      (nächster Empfänger).

   Source Address
      Die Absenderadresse ist die MAC-Adresse des Vorherigen
      Netzwerkknoten (letzter Absender).

   Length
      Gibt Auskunft über die Grösse der Nutzlast (in Bytes).

   Data / Payload
      Nun folgt die eigentliche Nutzlast. Die Grösse ist mittels der
      Length vorgegeben.

   Frame Checksum (CRC)
      Letztlich gibt es noch eine Prüfsumme des ganzen Frames.