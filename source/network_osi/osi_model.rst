==========
OSI Modell
==========

Netzzugang
==========

Bitübertragungsschicht
----------------------

Die erste Schicht basiert auf physischen oder elektrischen Prinzipien,
welche die Übertragung von Binärdatenströmen auf Hardwareebene erlaubt.

So sind also Netzwerkkabel und Repeater ein wesentlicher Teil dieser
Schicht.

.. _networking-osi_model-layer_2:

Sicherungsschicht
-----------------

Die Sicherungsschicht definiert erstmalig die grobe Struktur der
Datenströme.

Dies ist hier der sogenannte MAC-Frame oder Ethernet-Frame, in welchen
dann der tatsächliche Inhalt (bzw. Nutzlast) eingebettet wird.

Für Informationen und Spezifikationen zum MAC-Frame, siehe
:doc:`ethernet_frame`.

Die Aufgabe der Sicherungsebene ist es, den erfolgreichen Übertrag
eines Streams zu gewährleisten.

Komponenten dieser Schicht sind beispielsweise :doc:`Bridges <../network_dev/bridge>`,
:ref:`networking-switch-l2_switches` oder
:doc:`Access Points <../network_dev/access-point>`, da diese Verwendung von den
Informationen aus dem MAC-Frame machen.

Internet
========

Vermittlungsschicht
-------------------

Die Vermittlungsschicht ermöglicht die Kommunikation über mehrere
Netzwerke und tut dies mittels IP Adressen.

Diese Schicht definiert ebenfalls Informationen die an die Nutzlast
angefügt wird (hier: IP-Header).

Für Informationen und Spezifikationen zum IP-Header, siehe
:doc:`ip_header`.


Teil der Vermittlungsebene sind beispielsweise :doc:`Router <../network_dev/router>`
und :ref:`networking-switch-l3_switches`.

Transport
=========

Transportsschicht
-----------------

Die Transportsschicht behandelt die verschiedenen Protokolle und
erlaubt mittels Portnummern mehrere Services unter der selben
IP Adresse.

Der Header der Transportsschicht hängt vom verwendeten Protokoll ab.
Die wohl bekanntesten Protokolle hier sind TCP und UDP.

Details zum Aufbau sind hier zu finden: :doc:`tcp_segment`, :doc:`udp_segment` 


Anwendungen
===========

Sitzungsschicht
---------------

*Platzhalter*

Darstellungsschicht
-------------------

*Platzhalter*

Anwendungsschicht
-----------------

*Platzhalter*