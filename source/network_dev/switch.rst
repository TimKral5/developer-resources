======
Switch
======

Eine passende Analogie für einen Switch ist wohl ein Kreisel aus dem
Strassenverkehr, bei welchem jede Zufahrt auch eine Ausfahrt ist, und
jede Ausfahrt eine feste Anschrift hat.

Die Zu-/Ausfahrten sind in diesem Beispiel vergleichbar mit den
Anschlüssen an einem Switch, während die Anschriften mit MAC-Adressen
vergleichbar sind.

Man schickt also ein Paket, welches eine angegebene Zieladresse (MAC)
besitzt, an einen Switch, und es verlässt diesen durch den richtigen
Port.

.. _networking-switch-l2_switches:

L2 Switches
===========

Switches in ihrer Grundausstattung sind Teil der
:ref:`networking-osi_model-layer_2`. Diese nennen sich L2 Switches
(Layer 2 Switches) und dienen dem einfachen Umleiten von Paketen.
Sie tun dies mittels der MAC-Adressen in den Paketheadern und
sogenannten :ref:`Switching Tabellen
<networking-terminology-switching_table>`.

Diese Tabelle wird, wenn eine Anfrage durch den Switch
geleitet wird, populiert, indem der Switch eine Broadcast-Anfrage mit
der gesuchten MAC Adresse tätigt. Die Antwort kommt schliesslich aus
der Richtung, in welche die Pakete verschickt werden müssen. Letztlich
wird diese Route in der Switching Tabelle eingetragen.

.. _networking-switch-l3_switches:

L3 Switches
===========

Ein L3-Switch baut auf dem Konzept der L2-Switches auf, erweitert diese
jedoch um Funktionen, welche Informationen aus den IP-Headern
verwenden.

Jene Funktionen können VLAN Kompatibilität, NAT Funktionalität, oder
die Zusammenarbeit beziehungsweise Verschmelzung mit einem Router
sein.