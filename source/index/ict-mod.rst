===================
ICT Kompetenzraster
===================

.. attention::
   In Bearbeitung!!

Begleiten von ICT-Projekten (A)
===============================

* *Platzhalter*

Unterstützen und Beraten im ICT-Umfeld (B)
==========================================

Den eigenen ICT-Arbeitsplatz einrichten (B1)
--------------------------------------------

.. rubric:: Computer mit Betriebssystem aufsetzten (B1.1)

* :doc:`../operating_systems/index`

.. rubric:: Einrichtung und Test der Netzwerkverbindung (B1.2)

* :doc:`../networking/net-dev-index`
* :doc:`../network_osi/index`
* :doc:`../networking/terminology`

.. rubric:: Sicherheitsmassnahmen konfigurieren (Firewall, Antivirus, etc.) (B1.3)

* *Platzhalter*

.. rubric:: Software und Updates (B1.4)

* *Platzhalter*

.. rubric:: Peripheriegeräte (B1.5)

* *Platzhalter*

.. rubric::  Bürotisch und Bürostuhl (B1.6)

* *Platzhalter*

Beratung in Bezug auf Datenschutz und Datensicherheit (B3)
----------------------------------------------------------

.. rubric:: Antwort auf gezielte Fragen zu System, Netzwerk, Software
   und Daten (B3.1)

* :doc:`../security/protected_data`
* :doc:`../security/protection`
* :doc:`../security/terminology`

.. rubric:: Informieren über Gefahren im Netz und Umgang mit
   schützenswerten Daten (B3.2)

* :doc:`../security/protected_data`
* :doc:`../security/protection`

.. rubric:: Empfehlung Schutzmassnahmen (B3.3)

* :doc:`../security/protection`

Aufbauen und Pflegen von digitalen Daten (C)
============================================

* *Platzhalter*

Entwickeln von Applikationen (G)
================================

* *Platzhalter*

Ausliefern und Betreiben von Applikationen (H)
==============================================

* *Platzhalter*
