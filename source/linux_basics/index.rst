============
Linux Basics
============

Linux ist ein Betriebssystem, dass aus dem ursprünglichen Unix
hervorging. Es ist Open Source (`GitHub
<https://github.com/torvalds/linux>`_) und wird bis heute aktiv von der
Community weiterentickelt.

Grundlegende Beiträge
=====================

.. toctree:: 
   :maxdepth: 1
   :glob:

   install_linux
   user_management