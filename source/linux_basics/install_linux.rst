==================
Linux Installieren
==================

.. rubric:: Vermerk

Bei Linux sind gegenüber der Alternativen Windows und MacOS wichtige
Unterschiede, dass Linux kostenlos ist und auf absolute Freiheit auf
dem eigenen System setzt, sowie auf praktisch jedem Gerät laufen kann
(alt und neu).

Es ist hier auch wichtig anzumerken, dass, abgesehen vom Gerät und dem
Speichermedium (USB oder CD), alle Schritte absolut kostenfrei sind.

Einführung
==========

Ein wichtiger Unterschied zwischen Linux und seinen bekanntesten
Konkurrenten ist, dass Linux in vielen Formen daher kommt.

Die sogenannten Linux Distributionen erweitern den Linux Kernel
mithilfe von Paketen und bereiten diese für bestimmte Anwendungszwecke
vor. Siehe `Linux Distribution Timeline
<https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg>`_
für eine Übersicht über eine Handvoll der Distributionen.

Auswahl Linux Distribution
==========================

Die Auswahl der Distribution ist ein sehr individueller Prozess, da
diese sehr genau anhand der persönlichen Präferenzen getroffen werden
kann, und, da es, wie zuvor erwähnt, für praktisch jeden Zweck eine
ordendliche Menge an passenden Systemen gibt.

Eine grundlegende Regel ist, dass man als Einsteiger darauf bedacht
sein sollte, eine Distribution zu wählen, welche eine umfangreiche
grafische Oberfläche besitzt, die die Verwendung des Terminals nicht
unbedigt voraussetzt.

So sollen also beispielsweise die Einstellungen, die Installation von
Programmen oder die alltägliche Büroarbeit über die grafische Obefläche
möglich sein.

.. rubric:: Empfehlungen

Ein möglicher Ansatz beim Einstieg in die Linux Welt ist die Verwendung
einer der drei grossen Distributionen, von welchen die meisten Anderen
abstammen:

* Arch (grundsätzlich eher für erfahrene Linux Nutzer)
* Debian (`Debian Homepage <https://www.debian.org>`_)
* Fedora (`Fedora Homepage <https://fedoraproject.org>`_)

Vorbereitung Installationsmedium
================================

Ist die Auswahl getroffen, erfolgt der Download der Installationsdatei.
Diese ist in der Regel auf der offiziellen Website der Community zu
finden und bietet grundsätzlich einen sehr leicht zu findenden
Download. Es gilt zu beachten, dass die richtige Prozessorarchitektur
ausgewählt wird.

Danach muss diese 'Imagedatei' auf ein Speichermedium gebrannt werden.
Dies tut man mittels eines Tools wie Rufus (Windows) oder Impression
(Linux).

Start auf Installationsmedium
=============================

Es folgt der Startvorgang auf das Speichermedium, was sich über das
sogenannte UEFI (bei alten Geräten noch BIOS), welches sich meist beim
Aufstarten des Gerätes über das Drücken einer bestimmten Taste
(oftmals Escape, F1, F2, F9, F10 oder F11) öffnen lässt.

Im UEFI / BIOS erfolgt der Aufruf des Bootmenüs, unter welchem dann
das Startmedium ausgewält werden kann. Hier wählt man also das
vorbereitete Installationsmedium aus, und speichert die Änderungen.

Installation
============

Der Installationsvorgang ist hier ebenfalls stark abhängig von der
gewählten Distribution, jedoch gibt ein paar Regelmässigkeiten.

Grundsätzlich wird bei jeder Installation eine Netzwerkkonfiguration
vorgenommen, die Partionen werden eingerichtet, der Root Nutzer wird
konfiguriert und wahlweise auch ein Standartbenutzer.