================
Nutzerverwaltung
================

Nutzer erstellen
================

.. rubric:: Linux Native

.. code-block:: bash

   adduser <user>

Nutzer entfernen
================

.. rubric:: Linux Native

.. code-block:: bash

   deluser <user>

Nutzerrechte bearbeiten
=======================

.. rubric:: Linux Native

.. code-block:: bash

   chmod [-Rcvf] [ugoa]{+|-|=}[rwxXst]

.. code-block:: text

   options:
    - R: recursive
    - c: list changed files
    - v: verbose
    - f: hide errors

   letters:
    - u: user / owner
    - g: group / owner's group
    - o: others
    - a: all
    - r: read access
    - w: write access
    - x: execution priviledge
    - X: ??
    - s: ??
    - t: ??


Nutzer zu den 'sudoers' hinzufügen
==================================

Befehle mit Root Berechtigungen ausführen
=========================================