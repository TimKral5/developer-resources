# Project information
project = 'Developer Resources'
copyright = '2024, Tim Kral'
author = 'Tim Kral'
version = 'v1.0.0'

# General configuration
extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = []

language = 'de'

# Options for HTML output

# html_theme = 'alabaster'
html_theme = 'bizstyle'
html_static_path = ['_static']

# Options for PDF output
# latex_theme = "howto"