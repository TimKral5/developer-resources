========================
Einstieg Betriebssysteme
========================

Grundlegendes
=============

Was ist ein Betriebssystem?
---------------------------

Der Begriff des Betriebssystems beschreibt eine Platform, welche direkt
auf der Hardware läuft und den Umgang mit dieser vereinfacht.

Ein Betriebssystem tut dies, indem sie sich mit dem Download und der
Verwaltung von Hardware Treibern rumschlägt, Dateisysteme verwaltet,
übergeordnete Software verwaltet und ausführt und dem Nutzer eine
Nutzeroberfläche zur Verfügung stellt (grafisch oder textbasiert,
etc.).

Gängige Betriebssysteme
=======================

Die wohl bekanntesten Betriebssysteme sind Linux, MacOS und Windows.