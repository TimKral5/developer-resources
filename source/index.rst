===================
Developer Resources
===================

.. note:: 
   | An Lehrpersonen und andere offizielle Stellen:

   Die unten aufgeführten Abschnitte "ICT Kompetenzraster" und
   "ICT Module" sollen Übersicht über die Inhalte bieten.

   | Mit freundlichen Grüssen
   | Tim Kral

.. rubric:: Abschnitte

* :doc:`index/index`
* :doc:`index/ict-mod`

.. toctree::
   :maxdepth: 3
   :hidden:

   index/index

.. toctree::
   :maxdepth: 1
   :hidden:
   
   index/ict-mod