==============
Normalisierung
==============

.. glossary:: 

   Erste Normalform (1NF)

      Die erste Normalform ist gegeben, wenn alle Spalten einer Tabelle
      nicht mehr als eine Information beinhalten.

      Ein Beispiel:

      +----+----------------------------+
      | ID | Address                    |
      +====+============================+
      | 1  | Musterstrasse 1, Musterort |
      +----+----------------------------+

      Das Problem hier besteht darin, dass die Adressen kombiniert
      statt einzeln gespeichert werden, was die Analysierbarkeit
      einschränkt und die Optimierung der Abspeicherung mittels
      Auslagerung erschwert.

      Eine (keineswegs die einzige) Lösung dazu:

      +----+---------------+--------------+-----------+
      | ID | Street Name   | House Number | City      |
      +====+===============+==============+===========+
      | 1  | Musterstrasse | 1            | Musterort |
      +----+---------------+--------------+-----------+

      Hier erhält nun jede Information ihre einzige Spalte, was die
      zuvor genannten Probleme auflöst.

   Zweite Normalform (2NF)

      Die zweite Normalform sieht vor, dass die erste gegeben ist. Sie
      setzt jedoch ebenfalls voraus, dass im Verhältnis stehende
      Informationen mittels eines eindeutigen Primärschlüssels
      auszumachen sind.

   Dritte Normalform (3NF)

      Letztlich, die dritte Normalform setzt wiederum die vorherigen
      Begebenheiten voraus und erweitert diese. Sie setzt also voraus,
      dass keine Redundanz in den Daten besteht, was zur Optimierung
      der Datenspeicherung verhelfen kann.