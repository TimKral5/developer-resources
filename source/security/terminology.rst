================================================
Begriffe rund um Datenschutz und Datensicherheit
================================================

Grundlegende Begriffe
=====================

Datenschutz
   Bezieht sich auf die Gesetzeslage rund um kritische Daten.

Datensicherheit
   Bezieht sich auf den technischen Hintergrund, also auf Massnahmen,
   um Daten zu schützen.