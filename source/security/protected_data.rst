===================
Schutzwürdige Daten
===================

Viele Informationen über eine Person sind in den Augen des Gesetzes
besonders schützenswert und müssen gegen jedwede Form von unbefugtem
Zugriff und / oder Manupulation geschützt werden.

Informationen zu möglichen Schutzmassnahmen sind :doc:`hier <protection>` zu finden.

Personenbezogene Daten
======================

Informationen, wie Namen, Geburtsdaten, Adressen, Telefonnummern oder
Sozialsversicherungsnummern gelten als schützenswert, da
diese sich auf identifizierbare natürliche Personen beziehen.


Gesundheitsdaten
================

Gesunheitsdaten sind ebenfalls heikel, da diese massgeblichen Einfluss
auf die Wahrnehmung einer Person haben können, und somit sind
Informationen über den Gesundheitszustand einer Person, medizinische
Diagnosen, Behandlungen, Medikamentenhistorie und andere
gesundheitsbezogene Details, ebenfalls als besonders schützenswert zu
erachten.


Finanzdaten
===========

Finanzdaten sind genauso schützenswert, da diese sowohl Einfluss auf
die öffentliche Wahrnehmung der Person haben können, als auch grosses
Missbrauchpotenzial haben, da diese beispielsweise den Zugriff auf die
Vermögenswerte einer Person haben könnten.

So gelten also Kreditkartendaten, Bankkontoinformationen,
Einkommensdetails und andere finanzielle Informationen, die für
Betrügereien und Identitätsdiebstahl anfällig sein können, als
besonders schützenswert.

Biometrische Daten
==================

Fingerabdrücke, Retina-Scans, Gesichtserkennungsdaten und andere
biometrische Merkmale, die zur Identifikation von Personen verwendet
werden, sind auch besonders schützenswert.


Unternehmensgeheimnisse
=======================

Da Unternehmensgeheimnisse wettbewerbentscheidend sein können, fallen
diese genauso unter die Kategorie "schützenswert". Hier einige
Beispiele:

Strategische Geschäftspläne, Forschungs- und Entwicklungsdaten,
vertrauliche Verträge und andere proprietäre Informationen von
Unternehmen.


Juristische Informationen
=========================

Im Weiteren sind, Rechtsanwaltskommunikation, Gerichtsakten und
andere rechtliche Dokumente, die sensible Informationen über
Rechtsstreitigkeiten enthalten, ebenfalls besonders schützenswert.


Ethnische oder ethnologische Daten
==================================

Ethnische oder ethnologische Daten, Informationen über die ethnische
Herkunft einer Person, werden ebenfalls als besonders schützenswert
angesehen.


Kinderdaten
===========

Letztlich, Informationen über Minderjährige, die aufgrund ihrer
besonderen Schutzbedürftigkeit als besonders sensibel gelten, sind auch
besonder schützenswert.