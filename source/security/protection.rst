================
Schutzmassnahmen
================

Verlustsschutz
==============

Ist man im Besitz wichtiger Informationen und möchte sicherstellen,
dass diese nicht gelöscht werden, oder auf eine andere Art und Weise
korrumpiert werden, dann gibt es eine Hand voll Möglichkeiten, dies zu
tun.

Backups
-------

Backups sind Zweitsicherungen, also Kopien eines Datensatzes, welche
dem Zweck dienen, im Falle des Verlustes der Originaldaten die
Möglichkeit der Wiederherstellung des Datensatzes auf den Zeitpunkt
des letzten durchgeführten Backups zu ermöglichen.

Ihrem Zweck zur Folge, sind diese meist, auf einem vom ursprüchlichen
Gerät unabhängigen System, gesichert.

Einfache Kopie
^^^^^^^^^^^^^^

Die wohl einfachste Form eines Backups ist eine simple eins-zu-eins
Kopie der Dateien an einen anderen Ort.

Git Versionsmanagement
^^^^^^^^^^^^^^^^^^^^^^

Das Problem bei einfachen Kopien von Dateien ist, dass sowohl die
Dateien, welche zwischen verschiedenen Schnappschüssen verändert
wurden, wie auch jene, welche nicht bearbeitet wurden, mehrfach
gespeichert werden.

Git bietet hier eine Lösung, da hier bei Schnappschüssen lediglich die
Änderungen im Vergleich zum jeweils Vorherigen gespeichert werden.

Zugriffsschutz
==============

Verschlüsselung
---------------

Um die Sicherheit der eigenen Daten selbst bei unbefugtem Lesen
sicherzustellen, können diese verschlüsselt werden. Dies bedeutet, dass
diese mittels eines Schlüssels unleserlich gemacht werden, sodass diese
möglichst nur mit dem selben Schlüssel oder oftmals auch nur mit einem
passenden Gegenstück entschlüsselt werden können.

Symmetrische Verschlüsselung
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die symmetrische Verschlüsselung verwendet lediglich einen Schlüssel,
mit welchem die Nachricht ver- und entschlüsselt werden kann.

Der AES (Advanced Encryption Standard) Algorithmus ist beispielsweise
symmetrisch. Für Details über die Implementation, siehe
[`RFC 3826 <https://www.rfc-editor.org/rfc/rfc3826>`_].


Asymmetrische Verschlüsselung
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die asymmetrische Verschlüsselung unterscheidet sich in sofern von der
symmetrischen Verschlüsselung, als dass diese zwei Schlüssel verwendet,
also ein Schlüssel dient der Verschlüsselung und ein Schlüssel dient
der Entschlüsselung. Es gilt hier, dass die Schlüssel jeweils nur für
einen der Zwecke geeignet sind, und demnach nicht 

Der RSA (Rivest, Shamir, Adleman) Algorithmus ist beispielsweise
asymmetrisch. Für Details über die Implementation, siehe
[`RFC 8017 <https://www.rfc-editor.org/rfc/rfc8017>`_].