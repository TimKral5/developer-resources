html_build=dirhtml
html_build_dir=build/${html_build}

html_out_dir=pub/html

make ${html_build}

cp -rf ${html_build_dir} ${html_out_dir}