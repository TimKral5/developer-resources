pdf_build=latexpdf
pdf_build_dir=build/latex

pdf_out_dir=pub/pdf

rm -rf ${pdf_out_dir}
mkdir -p ${pdf_out_dir}

make ${pdf_build}

cp -rf ${pdf_build_dir}/*.pdf ${pdf_out_dir}