apt_opt=-y

apt update
apt upgrade

apt install ${apt_opt} nodejs
apt install ${apt_opt} npm

npm install -g serve

apt install ${apt_opt} python3
apt install ${apt_opt} python3-sphinx
apt install ${apt_opt} python3-pip
apt install ${apt_opt} make

# pip install --break-system-packages rst2pdf
# pip install --break-system-packages sphinx-intl

pip install rst2pdf
pip install sphinx-intl